﻿using System;
using Gryps.StorageProviders;
using Gryps.StorageProviders.Oracle;

namespace Gryps.Tests
{
    internal static class TestsConfiguration
    {
        public const string OracleConnectionString = "Data Source=CZAJ;User Id=CZAJ;Password=CZAJ;";

        public static readonly Func<IStorageProvider> DefaultProvider = () => new OracleStorageProvider(OracleConnectionString);
    }
}
