﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Gryps.StorageProviders;
using Gryps.StorageProviders.Oracle;
using NUnit.Framework;

namespace Gryps.Tests.StorageProviders.Oracle
{
    [TestFixture]
    public class OracleStorageProviderTests
    {
        [SetUp]
        public void SetUp()
        {
            using (var provider = this.CreateProvider())
            {
                provider.Truncate();
            }
        }

        [Test]
        public void StreamExists_ReturnsFalseWhenStreamDoesntExist()
        {
            using (var provider = this.CreateProvider())
            {
                Assert.IsFalse(provider.StreamExists("NOT_EXISTING_STREAM"));
            }
        }

        [Test]
        public void StreamExists_ReturnsTrueWhenStreamExists()
        {
            using (var provider = this.CreateProvider())
            {
                provider.InsertStream("TEST", 0);
                Assert.IsTrue(provider.StreamExists("TEST"));
            }
        }

        [Test]
        public void InsertStream_InsertsNewStreamIfStreamDoesntExist()
        {
            using (var provider = this.CreateProvider())
            {
                provider.InsertStream("TEST", 0);
                Assert.IsTrue(provider.StreamExists("TEST"));
            }
        }

        [Test]
        public void GetStreamVersion_ReturnsNullWhenStreamDoesntExist()
        {
            using (var provider = this.CreateProvider())
            {
                Assert.IsNull(provider.GetStreamVersion("TEST"));
            }
        }

        [Test]
        public void GetStreamVersion_ReturnsCurrentVersionIfStreamExists()
        {
            using (var provider = this.CreateProvider())
            {
                provider.InsertStream("TEST", 7);
                Assert.AreEqual(7, provider.GetStreamVersion("TEST"));
            }
        }

        [Test]
        public void InsertStream_ThrowsExceptionWhenStreamAlreadyExists()
        {
            using (var provider = this.CreateProvider())
            {
                Assert.Throws<StorageProviderException>(() =>
                {
                    provider.InsertStream("TEST", 0);
                    provider.InsertStream("TEST", 0);
                });                
            }
        }

        [Test]
        public void UpdateStream_ReturnsFalseWhenStreamDoesntExist()
        {
            using (var provider = this.CreateProvider())
            {
                Assert.IsFalse(provider.UpdateStream("NOT_EXISTING", 0, 5));
            }
        }

        [Test]
        public void UpdateStream_ReturnsFalseWhenStreamVersionDoesntMatch()
        {
            using (var provider = this.CreateProvider())
            {
                provider.InsertStream("EXISTING", 5);
                Assert.IsFalse(provider.UpdateStream("NOT_EXISTING", 0, 5));
            }
        }

        [Test]
        public void UpdateStream_ReturnsTrueAndUpdatesStreamWhenStreamExistsAndIsInCorrectVersion()
        {
            using (var provider = this.CreateProvider())
            {
                provider.InsertStream("TEST", 7);
                Assert.IsTrue(provider.UpdateStream("TEST", 7, 9));
            }
        }

        [Test]
        public void InsertEvents_InsertsEvents()
        {
            using (var provider = this.CreateProvider())
            {
                provider.InsertStream("TEST", 0);
                provider.UpdateStream("TEST", 0, 3);
                provider.InsertEvents("TEST", 0, new[]
                {
                    new EventData("T1"),
                    new EventData("T2", ""),
                    new EventData("T3", "Test data"),
                });

                var events = provider.GetEvents("TEST", 0).ToList();

                Assert.AreEqual(3, events.Count);

                Assert.AreEqual(1, events[0].Version);
                Assert.AreEqual("T1", events[0].EventType);
                Assert.AreEqual("TEST", events[0].StreamName);
                Assert.AreEqual("", events[0].DataToString());

                Assert.AreEqual(2, events[1].Version);
                Assert.AreEqual("T2", events[1].EventType);
                Assert.AreEqual("TEST", events[1].StreamName);
                Assert.AreEqual("", events[1].DataToString());

                Assert.AreEqual(3, events[2].Version);
                Assert.AreEqual("T3", events[2].EventType);
                Assert.AreEqual("TEST", events[2].StreamName);
                Assert.AreEqual("Test data", events[2].DataToString());
            }
        }    

        private IStorageProvider CreateProvider()
        {
            return TestsConfiguration.DefaultProvider();
        }
    }
}
