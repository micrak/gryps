﻿using System;
using System.Linq;
using Gryps.StorageProviders;
using NUnit.Framework;

namespace Gryps.Tests
{
    [TestFixture]
    public class StreamRepositoryTests
    {
        [SetUp]
        public void SetUp()
        {
            using (var provider = this.CreateProvider())
                provider.Truncate();
        }

        [Test]
        public void PushEventsTest()
        {
            var repository = this.CreateRepository();
            var aggregate = Guid.NewGuid().ToString();
            Assert.IsTrue(repository.PushEvents(aggregate, 0, new[]
            {
                new EventData("T1", "Val1"),
                new EventData("T2", "Val2"),
                new EventData("T3", "Val3"),
            }));

            Assert.AreEqual(3, repository.GetStreamVersion(aggregate));

            Assert.IsFalse(repository.PushEvents(aggregate, 0, new[]
            {
                new EventData("T1", "Val1"),
            }));

            Assert.IsTrue(repository.PushEvents(aggregate, 3, new[]
            {
                new EventData("T1", "Val1"),
            }));

            Assert.AreEqual(4, repository.GetStreamVersion(aggregate));

            Assert.AreEqual(4, repository.GetEvents(aggregate).Count());
            Assert.AreEqual(4, repository.GetEvents(aggregate, 1).Count());
            Assert.AreEqual(3, repository.GetEvents(aggregate, 2).Count());
            Assert.AreEqual(2, repository.GetEvents(aggregate, 3).Count());
            Assert.AreEqual(1, repository.GetEvents(aggregate, 4).Count());
            Assert.AreEqual(0, repository.GetEvents(aggregate, 5).Count());
            Assert.AreEqual(0, repository.GetEvents(aggregate, 6).Count());
        }

        [Test]
        public void FreeStyleTest()
        {
            var repository = this.CreateRepository();
        }

        private StreamRepository CreateRepository()
        {
            return new StreamRepository(this.CreateProvider);
        }

        private IStorageProvider CreateProvider()
        {
            return TestsConfiguration.DefaultProvider();
        }
    }
}
