﻿using System;

namespace Gryps.StorageProviders
{
    public class StorageProviderException : Exception
    {
        public StorageProviderException(string message) : base(message)
        {
        }

        public StorageProviderException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
