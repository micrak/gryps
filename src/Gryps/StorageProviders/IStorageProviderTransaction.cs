﻿using System;

namespace Gryps.StorageProviders
{
    public interface IStorageProviderTransaction : IDisposable
    {
        void Commit();

        void Rollback();
    }
}
