using System;
using System.Collections.Generic;

namespace Gryps.StorageProviders
{
    public interface IStorageProvider : IDisposable
    {
        IStorageProviderTransaction BeginTransaction();

        bool StreamExists(string name);
        void InsertStream(string name, long initialVersion);
        bool UpdateStream(string name, long oldVersion, long newVersion);
        long? GetStreamVersion(string streamName);

        void InsertEvents(string streamName, long startFromVersion, EventData[] eventDatas);
        IEnumerable<QueryEventData> GetEvents(string streamName, long startFromVersion);

        void Truncate();
    }
}