﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;

namespace Gryps.StorageProviders.Oracle
{
    internal sealed class OracleStorageProvider : IStorageProvider
    {
        private readonly Lazy<OracleConnection> _connection;

        public OracleStorageProvider(string connectionString)
        {
            Ensure.IsNotNullOrEmpty(connectionString, nameof(connectionString));

            this._connection = new Lazy<OracleConnection>(
                () =>
                {
                    var c = new OracleConnection(connectionString);
                    c.Open();
                    return c;
                });
        }

        public IStorageProviderTransaction BeginTransaction()
        {
            return new OracleStorageProviderTransaction(this.GetConnection());
        }

        public bool StreamExists(string name)
        {
            Ensure.IsNotNullOrEmpty(name, nameof(name));

            try
            {
                using (var command = this.CreateCommand(this.GetConnection()))
                {
                    command.CommandText = "SELECT COUNT(*) FROM STREAM WHERE NAME=:NAME";
                    command.Parameters.Add(new OracleParameter("NAME", OracleDbType.Varchar2, name, ParameterDirection.Input));
                    return (decimal)command.ExecuteScalar() > 0;
                }
            }
            catch (Exception exception)
            {
                throw new StorageProviderException(exception.Message, exception);
            }
        }

        public void InsertStream(string name, long initialVersion)
        {
            Ensure.IsNotNullOrEmpty(name, nameof(name));

            try
            {
                using (var command = this.CreateCommand(this.GetConnection()))
                {
                    command.CommandText = "INSERT INTO STREAM (NAME, VERSION) VALUES (:NAME, :VERSION) RETURNING SYSSTREAM INTO :SYSSTREAM";
                    command.Parameters.Add(new OracleParameter("NAME", OracleDbType.Varchar2, name, ParameterDirection.Input));
                    command.Parameters.Add(new OracleParameter("VERSION", OracleDbType.Decimal, initialVersion, ParameterDirection.Input));
                    command.Parameters.Add(new OracleParameter("SYSSTREAM", OracleDbType.Decimal, ParameterDirection.ReturnValue));
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception exception)
            {
                throw new StorageProviderException(exception.Message, exception);
            }
        }

        public bool UpdateStream(string name, long oldVersion, long newVersion)
        {
            Ensure.IsNotNullOrEmpty(name, nameof(name));

            try
            {
                using (var command = this.CreateCommand(this.GetConnection()))
                {
                    command.BindByName = true;
                    command.CommandText = "UPDATE STREAM SET VERSION=:NEW_VERSION WHERE VERSION=:OLD_VERSION AND NAME=:NAME";
                    command.Parameters.Add(new OracleParameter("NAME", OracleDbType.Varchar2, name, ParameterDirection.Input));
                    command.Parameters.Add(new OracleParameter("OLD_VERSION", OracleDbType.Int64, oldVersion, ParameterDirection.Input));
                    command.Parameters.Add(new OracleParameter("NEW_VERSION", OracleDbType.Int64, newVersion, ParameterDirection.Input));
                    return command.ExecuteNonQuery() > 0;
                }
            }
            catch (Exception exception)
            {
                throw new StorageProviderException(exception.Message, exception);
            }
        }

        public long? GetStreamVersion(string streamName)
        {
            Ensure.IsNotNullOrEmpty(streamName, nameof(streamName));

            try
            {
                using (var command = this.CreateCommand(this.GetConnection()))
                {
                    command.CommandText = "SELECT VERSION FROM STREAM WHERE NAME=:NAME";
                    command.Parameters.Add(new OracleParameter("NAME", OracleDbType.Varchar2, streamName, ParameterDirection.Input));

                    var scalar = command.ExecuteScalar();
                    return (long?) scalar;
                }
            }
            catch (Exception exception)
            {
                throw new StorageProviderException(exception.Message, exception);
            }
        }

        public IEnumerable<QueryEventData> GetEvents(string streamName, long startFromVersion)
        {
            Ensure.IsNotNullOrEmpty(streamName, nameof(streamName));

            try
            {
                using (var command = this.CreateCommand(this.GetConnection()))
                {
                    command.CommandText = @"SELECT E.TYPE, E.DATA, E.STREAMVERSION 
                                            FROM STREAMEVENT E 
                                            JOIN STREAM S ON S.SYSSTREAM=E.SYSSTREAM                                            
                                            WHERE S.NAME=:NAME AND E.STREAMVERSION>=:STARTFROMVERSION
                                            ORDER BY E.STREAMVERSION ASC";
                    command.Parameters.Add(new OracleParameter("NAME", OracleDbType.Varchar2, streamName, ParameterDirection.Input));
                    command.Parameters.Add(new OracleParameter("STARTFROMVERSION", OracleDbType.Int64, startFromVersion, ParameterDirection.Input));

                    using (var reader = command.ExecuteReader())
                    {
                        var result = new List<QueryEventData>();
                        while (reader.Read())
                        {
                            string type = reader.GetString(0);
                            long version = reader.GetInt64(2);

                            byte[] array;
                            using (var clob = reader.GetOracleClob(1))
                            {                   
                                array = new byte[clob.Length];
                                clob.Read(array, 0, array.Length);
                            }

                            result.Add(new QueryEventData(streamName, type, version, array));
                        }                        

                        return result;
                    }
                }
            }
            catch (Exception exception)
            {
                throw new StorageProviderException(exception.Message, exception);
            }
        }

        public void InsertEvents(string streamName, long startFromVersion, EventData[] eventDatas)
        {
            Ensure.IsNotNullOrEmpty(streamName, nameof(streamName));
            Ensure.IsNotNull(eventDatas, nameof(eventDatas));

            OracleClob[] clobs = null;
            try
            {
                var con = this.GetConnection();
                using (var command = this.CreateCommand(con))
                {
                    command.ArrayBindCount = eventDatas.Length;
                    command.CommandText = @"INSERT INTO STREAMEVENT 
                                                (SYSSTREAM, TYPE, DATA, STREAMVERSION) 
                                            VALUES 
                                                (
                                                    (SELECT SYSSTREAM FROM STREAM WHERE NAME=:NAME), 
                                                    :TYPE, 
                                                    :DATA, 
                                                    :STREAMVERSION
                                                )";

                    clobs = eventDatas.Select(g =>
                    {
                        var clob = new OracleClob(con, false, false);
                        var chars = GrypsConfiguration.Encoding.GetChars(g.Data);
                        clob.Write(chars, 0, chars.Length);
                        return clob;
                    }).ToArray();

                    var versions = new long[eventDatas.Length];
                    var writeVersion = startFromVersion + 1;
                    for (int i = 0; i < eventDatas.Length; i++)
                    {
                        versions[i] = writeVersion;
                        writeVersion++;
                    }

                    command.Parameters.Add(new OracleParameter("NAME", OracleDbType.Varchar2,
                        Enumerable.Repeat(streamName, eventDatas.Length).ToArray(), ParameterDirection.Input));
                    command.Parameters.Add(new OracleParameter("TYPE", OracleDbType.Varchar2,
                        eventDatas.Select(e => e.EventType).ToArray(), ParameterDirection.Input));
                    command.Parameters.Add(new OracleParameter("DATA", OracleDbType.Clob, clobs,
                        ParameterDirection.Input));
                    command.Parameters.Add(new OracleParameter("STREAMVERSION", OracleDbType.Decimal, versions,
                        ParameterDirection.Input));

                    command.ExecuteNonQuery();
                }
            }
            catch (Exception exception)
            {
                throw new StorageProviderException(exception.Message, exception);
            }
            finally
            {
                if (clobs != null)
                {
                    foreach (var clob in clobs)
                    {
                        clob.Dispose();
                    }
                }
            }
        }

        public void Truncate()
        {
            var tablesToClear = new[] { "STREAM", "STREAMEVENT" };
            foreach (var table in tablesToClear)
            {
                using (var command = this.CreateCommand(this.GetConnection()))
                {
                    command.CommandText = $"TRUNCATE TABLE {table}";
                    command.ExecuteNonQuery();
                }
            }
        }

        public void Dispose()
        {
            if (this._connection.IsValueCreated)
            {
                this._connection.Value.Dispose();
            }
        }

        private OracleCommand CreateCommand(OracleConnection oracleConnection)
        {
            var command = oracleConnection.CreateCommand();
            command.BindByName = true;
            return command;
        }

        private OracleConnection GetConnection()
        {
            return this._connection.Value;
        }
    }
}
