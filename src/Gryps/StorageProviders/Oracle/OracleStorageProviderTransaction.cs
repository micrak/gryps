﻿using System.Data;
using Oracle.ManagedDataAccess.Client;

namespace Gryps.StorageProviders.Oracle
{
    internal sealed class OracleStorageProviderTransaction : IStorageProviderTransaction
    {
        private readonly OracleTransaction transaction;

        public OracleStorageProviderTransaction(OracleConnection connection)
        {            
            Ensure.IsNotNull(connection, nameof(connection));

            this.transaction = connection.BeginTransaction(IsolationLevel.Serializable);
        }

        public void Commit()
        {
            this.transaction.Commit();
        }

        public void Rollback()
        {
            this.transaction.Rollback();
        }

        public void Dispose()
        {
            this.transaction.Dispose();
        }
    }
}
