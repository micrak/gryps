﻿using System;
using System.Collections.Generic;
using Gryps.StorageProviders;

namespace Gryps
{
    public sealed class StreamRepository
    {
        private readonly Func<IStorageProvider> _storageProviderFactory;

        public StreamRepository(Func<IStorageProvider> storageProviderFactory)
        {
            Ensure.IsNotNull(storageProviderFactory, nameof(storageProviderFactory));

            this._storageProviderFactory = storageProviderFactory;
        }

        public bool PushEvent(string streamName, long streamCurrentVersion, EventData eventData)
        {
            Ensure.IsNotNull(eventData, nameof(eventData));

            return this.PushEvents(streamName, streamCurrentVersion, new[] { eventData });
        }

        public bool PushEvents(string streamName, long streamCurrentVersion, EventData[] eventDatas)
        {
            Ensure.IsNotNullOrEmpty(streamName, nameof(streamName));
            Ensure.IsNotNull(eventDatas, nameof(eventDatas));

            using (var provider = this._storageProviderFactory())
            {
                using (var transaction = provider.BeginTransaction())
                {
                    try
                    {
                        if (!provider.StreamExists(streamName))
                        {
                            provider.InsertStream(streamName, eventDatas.Length);
                            provider.InsertEvents(streamName, 0, eventDatas);
                            transaction.Commit();
                            return true;
                        }

                        if (provider.UpdateStream(
                                streamName,
                                streamCurrentVersion,
                                streamCurrentVersion + eventDatas.Length))
                        {
                            provider.InsertEvents(streamName, streamCurrentVersion, eventDatas);
                            transaction.Commit();
                            return true;
                        }

                        return false;
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }

        public long? GetStreamVersion(string streamName)
        {
            using (var provider = this._storageProviderFactory())
            {
                return provider.GetStreamVersion(streamName);
            }
        }

        public IEnumerable<QueryEventData> GetEvents(string streamName, long startFromVersion = 0)
        {
            using (var provider = this._storageProviderFactory())
            {
                return provider.GetEvents(streamName, startFromVersion);
            }
        }
    }
}
