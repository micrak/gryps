﻿namespace Gryps
{
    public sealed class EventData
    {
        public EventData(string eventType)
            : this(eventType, new byte[0])
        {            
        }

        public EventData(string eventType, string data)
            : this(eventType, data != null ? GrypsConfiguration.Encoding.GetBytes(data) : new byte[0])
        {            
        }

        public EventData(string eventType, byte[] data)
        {            
            Ensure.IsNotNullOrEmpty(eventType, nameof(eventType));
            Ensure.IsNotNull(data, nameof(data));

            this.EventType = eventType;
            this.Data = data;
        }

        public string EventType { get; }

        public byte[] Data { get; }
    }
}
