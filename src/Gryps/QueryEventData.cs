﻿using System;

namespace Gryps
{
    public sealed class QueryEventData
    {
        public QueryEventData(string streamName, string eventType, long version, byte[] data)
        {            
            Ensure.IsNotNullOrEmpty(streamName, nameof(streamName));
            Ensure.IsNotNullOrEmpty(eventType, nameof(eventType));
            Ensure.IsNotNull(data, nameof(data));

            this.StreamName = streamName;
            this.EventType = eventType;
            this.Data = data;
            this.Version = version;
        }

        public string StreamName { get; private set; }

        public string EventType { get; private set; }

        public byte[] Data { get; private set; }

        public long Version { get; private set; }

        public string DataToString()
        {
            return GrypsConfiguration.Encoding.GetString(this.Data);            
        } 
    }
}
