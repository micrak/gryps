﻿using System;

namespace Gryps
{
    public static class Ensure
    {
        public static void IsNotNull(object param, string paramName)
        {
            if (param == null)
                throw new ArgumentNullException(paramName);
        }

        public static void IsNotNullOrEmpty(string param, string paramName)
        {
            if (string.IsNullOrEmpty(param))
                throw new ArgumentException($"The '{paramName}' parameter cannot be null or empty.", paramName);
        }
    }
}
