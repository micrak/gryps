﻿using Gryps.StorageProviders.Oracle;

namespace Gryps
{
    public static class StreamRepositoryFactory
    {
        public static StreamRepository ForOracle(string connectionString)
        {
            return new StreamRepository(() => new OracleStorageProvider(connectionString));
        }
    }
}
